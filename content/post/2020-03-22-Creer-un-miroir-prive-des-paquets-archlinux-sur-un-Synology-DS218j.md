+++
title = "Créer un miroir privé des paquets ArchLinux sur un Synology DS218j"
tags = ["mirror", "archlinux", "linux","synology","ds218j", "nas"]
categories = ["Reprendre le contrôle"]
author = "Andy Costanza"
comments = true
layout = "post"
date = "2020-03-22T16:15:50+01:00"
+++
Confinement oblige, les réseaux qui font ce qu'internet est aujourd'hui sont extrêmement sollicités ces derniers jours. Selon [DE-CIX](https://www.de-cix.net/en/locations/germany/frankfurt/statistics), nous avons des pics journaliers à plus de 9 Terabits/sec

![](https://www.de-cix.net/traffic_FRA-1month-1170-400.png)

Je vous laisse deviner, à quelles heures sont ces pics ?

![](https://www.de-cix.net/traffic_FRA-2days-1170-400.png)

C'est exactement les heures pendant lesquels je suis sur mes pc ArchLinux et les moments où j'ai tendances à faire les mises à jour ou installer de nouvelles applications.

J'ai justement un [Synology DS218j](https://amzn.to/2WBYPMb) que j'utilise pour archiver mes photos dans le cadre de [mon activité complémentaire](https://andycostanza.com) et il lui reste encore quelques Go de disponible pour héberger un petit miroir privé des paquets ArchLinux.

## Configuration du Synology
Je pars du postulat que vous avez déjà un Synology monté, configuré et fonctionnel.

- Créez un nouveau dossier partagé sur votre volume

![](/images/2020-03-22/01.png)
- Créez 2 répertoires : repo et tmp

![](/images/2020-03-22/02.png)
- Installez __Web Station__ si ce n'est déjà fait :
- Créez un __Virtual Host__ basé sur le port

![](/images/2020-03-22/03.png)


## Choisir le miroir ArchLinux le plus rapide
Sur votre machine : 
- Installez `pacman-contrib` si ce n'est déjà fait
- Utilisez __rankmirrors__ pour tester le miroir le plus proche de chez vous. La commande ci-dessous permet de tester les miroirs en Belgique, France, Hollande et Allemagne :
```bash
curl -s "https://www.archlinux.org/mirrorlist/?country=FR&country=NL&country=BE&country=GE&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 -
```
- Parmi les 5 plus rapide, vérifiez lesquels sont compatibles avec __rsync__ sur le site [www.archlinux.org/mirrors](https://www.archlinux.org/mirrors/). 

Dans mon cas c'est __archlinux.mailtunnel.eu__ que j'utiliserai dans mon script de synchro 

## Script de synchro
- Connectez-vous en SSH sur votre Synology. ex: `ssh monUtilisateurSynology@192.168.1.2` si l'ip de votre NAS est 192.168.1.2
- En se basant sur [le script disponible sur wiki.archlinux.org](https://git.archlinux.org/infrastructure.git/tree/roles/syncrepo/files/syncrepo-template.sh), j'ai créé le script de synchro avec `vim /volume1/archlinux/syncrepo.sh` 
- J'ai ajouté les bons répertoires et les bonnes URLs vers le miroir que j'ai testé plus haut et voici à quoi le script ressemble :
```bash
#!/bin/bash
#
########
#
# Copyright © 2014-2019 Florian Pritz <bluewind@xinu.at>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
########
#
# This is a simple mirroring script. To save bandwidth it first checks a
# timestamp via HTTP and only runs rsync when the timestamp differs from the
# local copy. As of 2016, a single rsync run without changes transfers roughly
# 6MiB of data which adds up to roughly 250GiB of traffic per month when rsync
# is run every minute. Performing a simple check via HTTP first can thus save a
# lot of traffic.

# Directory where the repo is stored locally. Example: /srv/repo
target="/volume1/archlinux/repo"

# Directory where files are downloaded to before being moved in place.
# This should be on the same filesystem as $target, but not a subdirectory of $target.
# Example: /srv/tmp
tmp="/volume1/archlinux/tmp"

# Lockfile path
lock="/volume1/archlinux/syncrepo.lck"

# If you want to limit the bandwidth used by rsync set this.
# Use 0 to disable the limit.
# The default unit is KiB (see man rsync /--bwlimit for more)
bwlimit=0

# The source URL of the mirror you want to sync from.
# If you are a tier 1 mirror use rsync.archlinux.org, for example like this:
# rsync://rsync.archlinux.org/ftp_tier1
# Otherwise chose a tier 1 mirror from this list and use its rsync URL:
# https://www.archlinux.org/mirrors/
source_url='rsync://archlinux.mailtunnel.eu/archlinux/'

# An HTTP(S) URL pointing to the 'lastupdate' file on your chosen mirror.
# If you are a tier 1 mirror use: http://rsync.archlinux.org/lastupdate
# Otherwise use the HTTP(S) URL from your chosen mirror.
lastupdate_url='https://archlinux.mailtunnel.eu/lastupdate'


#### END CONFIG

[ ! -d "${target}" ] && mkdir -p "${target}"
[ ! -d "${tmp}" ] && mkdir -p "${tmp}"

exec 9>"${lock}"
flock -n 9 || exit

rsync_cmd() {
	local -a cmd=(rsync -rtlH --safe-links --delete-after ${VERBOSE} "--timeout=600" "--contimeout=60" -p --delay-updates --no-motd "--temp-dir=${tmp}")

	if stty &>/dev/null; then
		cmd+=(-h -v --progress)
	else
		cmd+=(--quiet)
	fi

	if ((bwlimit>0)); then
		cmd+=("--bwlimit=$bwlimit")
	fi

	"${cmd[@]}" "$@"
}

# if we are called without a tty (cronjob) only run when there are changes
if ! tty -s && [[ -f "$target/lastupdate" ]] && diff -b <(curl -Ls "$lastupdate_url") "$target/lastupdate" >/dev/null; then
	# keep lastsync file in sync for statistics generated by the Arch Linux website
	rsync_cmd "$source_url/lastsync" "$target/lastsync"
	exit 0
fi

rsync_cmd --exclude='*.links.tar.gz*' --exclude='/other' --exclude='/sources' --exclude='/iso' "${source_url}" "${target}"

#echo "Last sync was $(date -d @$(cat ${target}/lastsync))"
```
## Ajouter une tâche planifiée dans le Synology
- Toujours connecté en SSH à votre Synology, modifiez votre /etc/crontab : `sudo vim /etc/crontab`
- Ajoutez la tache de synchro pendant les heures creuses du trafic mondial :
```bash
0   5   *   *   *   root    bash /volume1/archlinux/syncrepo.sh
```

## Ajouter votre miroir privé dans votre /etc/pacman.d/mirrorlist
Sur votre machine :
- Modifiez votre `/etc/pacman.d/mirrorlist` et ajouter l'url vers votre NAS comme premier serveur dans la liste
```bash
Server = http://nas:9080/$repo/os/$arch
#Server = http://192.168.1.2:9080/$repo/os/$arch ou utilisez directement l'ip du NAS si l'url ci-dessus ne fonctionne pas
Server = ...
```

## Conclusion

Je sais que je ne suis qu'une goute d'eau dans cet océan de données qui transit par seconde mais si cet article peut donner des idées à d'autres, nous pourrons faire des rivières et celles-ci pourront, peut être, lisser le trafic mondial et soulager internet