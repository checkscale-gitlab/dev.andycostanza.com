+++
draft = true
title = "Créer son Maven archetype pour faciliter la création de ses nouvelles applications"
tags = ["maven","java","spring","spring boot"]
categories = ["dev"]
author = "Andy Costanza"
comments = true
layout = "post"
date = "2020-03-05T11:15:50+01:00"
+++
## Maven archetype, qu'est ce que c'est ?
Comme indiqué sur le [site officiel](http://maven.apache.org/guides/introduction/introduction-to-archetypes.html), Maven archetype est une boîte à outils de modélisation qui vous permettra de créer vos projets à partir de template préexistant ou de créer vos propres templates paramétrés et faciliter vos créations de nouveaux projets

En quelques secondes, un développeur peut disposer d'un projet Maven fonctionnel simplement en fournissant le groupId, l'artifactId et la version de l'archetype qu'il veut utiliser pour démarrer son projet.

```bash
$ mvn archetype:generate -DarchetypeGroupId=<groupId>
                        -DarchetypeArtifactId=<artifactId>
                        -DarchetypeVersion=<version>
```

Ce n'est pas plus compliqué que ça pour démarrer un projet depuis un archetype existant.

L'inconvénient, c'est qu'à l'heure où j'écris ces lignes, il existe plus de 2760 archetypes publiés sur maven central:

```bash
$ mvn archetype:generate 
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO]
[INFO] >>> maven-archetype-plugin:3.1.2:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO]
[INFO] <<< maven-archetype-plugin:3.1.2:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO]
[INFO]
[INFO] --- maven-archetype-plugin:3.1.2:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] No archetype defined. Using maven-archetype-quickstart (org.apache.maven.archetypes:maven-archetype-quickstart:1.0)
Choose archetype:
...
2756: remote -> us.fatehi:schemacrawler-archetype-plugin-lint (-)
2757: remote -> ws.osiris:osiris-archetype (Maven Archetype for Osiris)
2758: remote -> xyz.luan.generator:xyz-gae-generator (-)
2759: remote -> xyz.luan.generator:xyz-generator (-)
2760: remote -> za.co.absa.hyperdrive:component-archetype (-)
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): 
```
Difficile de retrouver ses jeunes avec toutes ces solutions. Heureusement, il existe la possibilité de filtrer les résultats comme ceci :

```bash
$ mvn archetype:generate -Dfilter=<groupId>:<artefactId>
```
Créons par exemple une simple application Spring Boot en recherchant les archetypes qui sont sous le groupId org.springframework et qui contiennent spring-boot dans leur arteficatId:
```bash
$ mvn archetype:generate -Dfilter=org.springframework:spring-boot
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO]
[INFO] >>> maven-archetype-plugin:3.1.2:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO]
[INFO] <<< maven-archetype-plugin:3.1.2:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO]
[INFO]
[INFO] --- maven-archetype-plugin:3.1.2:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] No archetype defined. Using maven-archetype-quickstart (org.apache.maven.archetypes:maven-archetype-quickstart:1.0)
Choose archetype:
1: remote -> org.springframework.boot:spring-boot-sample-actuator-archetype (Spring Boot Actuator Sample)
...
17: remote -> org.springframework.boot:spring-boot-sample-simple-archetype (spring-boot-sample-simple-archetype)
...
26: remote -> org.springframework.boot:spring-boot-sample-xml-archetype (Spring Boot XML Sample)
Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): : 17
Choose org.springframework.boot:spring-boot-sample-simple-archetype version:
1: 1.0.2.RELEASE
2: 1.0.2.BUILD-SNAPSHOT
Choose a number: 2: 1
Downloading from nexus: http://nexus.ucm.be/nexus/repository/maven-sec/org/springframework/boot/spring-boot-sample-simple-archetype/1.0.2.RELEASE/spring-boot-sample-simple-archetype-1.0.2.RELEASE.pom
Downloaded from nexus: http://nexus.ucm.be/nexus/repository/maven-sec/org/springframework/boot/spring-boot-sample-simple-archetype/1.0.2.RELEASE/spring-boot-sample-simple-archetype-1.0.2.RELEASE.pom (2.2 kB at 8.3 kB/s)
Downloading from nexus: http://nexus.ucm.be/nexus/repository/maven-sec/org/springframework/boot/spring-boot-sample-simple-archetype/1.0.2.RELEASE/spring-boot-sample-simple-archetype-1.0.2.RELEASE.jar
Downloaded from nexus: http://nexus.ucm.be/nexus/repository/maven-sec/org/springframework/boot/spring-boot-sample-simple-archetype/1.0.2.RELEASE/spring-boot-sample-simple-archetype-1.0.2.RELEASE.jar (6.3 kB at 24 kB/s)
Define value for property 'groupId': com.andycostanza
Define value for property 'artifactId': my-old-spring-boot-app
Define value for property 'version' 1.0-SNAPSHOT: :
Define value for property 'package' com.andycostanza: :
Confirm properties configuration:
groupId: com.andycostanza
artifactId: my-old-spring-boot-app
version: 1.0-SNAPSHOT
package: com.andycostanza
 Y: :
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: spring-boot-sample-simple-archetype:1.0.2.RELEASE
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.andycostanza
[INFO] Parameter: artifactId, Value: my-old-spring-boot-app
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.andycostanza
[INFO] Parameter: packageInPathFormat, Value: com/andycostanza
[INFO] Parameter: package, Value: com.andycostanza
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.andycostanza
[INFO] Parameter: artifactId, Value: my-old-spring-boot-app
[INFO] Project created from Archetype in dir: /home/andy/projects/my-old-spring-boot-app
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```
Nous venons de créer une très ancienne application Spring Boot (version 1.0.2 de 2014) que je vous déconseille d'utiliser car nous sommes passé à la version 2.2.x maintenant. Je pense que Spring a abandonné ces archetypes au détriment de leur site [Spring initializr](https://start.spring.io)

## Pourquoi créer son Maven archetype
Si comme moi, vous passez par Spring initializr pour créer vos projets mais que vous devez ensuite ajouter du "boilerplate" dans vos pom.xml, ajouter le fichier de config du CI, rajouter des fichiers properties de configuration, ou que sais-je encore. 

Grâce au Maven archetype, nous allons standardiser le développement d'applications en donnant à nos développeurs des templates prêt à l'emploi, déployés dans notre repository maven d'entreprise, en réduisant au strict minimum les tâches de configurations nécessaires au déploiement de celles-ci.

## Comment mettre en place un Maven archetype
### 1. Créer son archetype en utilsant l'archetype de création d'archetype
Drôle de titre de section, mais c'est pourtant le cas. [Maven a mis à notre disposition une dizaine d'archetypes](http://maven.apache.org/archetypes/index.html) pour mettre en place nos projets. L'un de ceux-ci permet la [création d'archetype maven](http://maven.apache.org/archetypes/maven-archetype-archetype/):
```bash
$ mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes
                        -DarchetypeArtifactId=maven-archetype-archetype
                        -DarchetypeVersion=1.4
```
Créons maintenant notre premier archetype qui sera utilisé comme référence pour nos futures applications Spring Boot 2.x
```bash
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------< org.apache.maven:standalone-pom >-------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] --------------------------------[ pom ]---------------------------------
[INFO]
[INFO] >>> maven-archetype-plugin:3.1.2:generate (default-cli) > generate-sources @ standalone-pom >>>
[INFO]
[INFO] <<< maven-archetype-plugin:3.1.2:generate (default-cli) < generate-sources @ standalone-pom <<<
[INFO]
[INFO]
[INFO] --- maven-archetype-plugin:3.1.2:generate (default-cli) @ standalone-pom ---
[INFO] Generating project in Interactive mode
[INFO] Archetype repository not defined. Using the one from [org.apache.maven.archetypes:maven-archetype-archetype:1.0-alpha-4] found in catalog remote
Define value for property 'groupId': com.andycostanza.archetypes
Define value for property 'artifactId': my-spring-boot-archetype
Define value for property 'version' 1.0-SNAPSHOT: :
Define value for property 'package' com.andycostanza.archetypes: :
Confirm properties configuration:
groupId: com.andycostanza.archetypes
artifactId: my-spring-boot-archetype
version: 1.0-SNAPSHOT
package: com.andycostanza.archetypes
 Y: :
[INFO] ----------------------------------------------------------------------------
[INFO] Using following parameters for creating project from Archetype: maven-archetype-archetype:1.4
[INFO] ----------------------------------------------------------------------------
[INFO] Parameter: groupId, Value: com.andycostanza.archetypes
[INFO] Parameter: artifactId, Value: my-spring-boot-archetype
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: package, Value: com.andycostanza.archetypes
[INFO] Parameter: packageInPathFormat, Value: com/andycostanza/archetypes
[INFO] Parameter: package, Value: com.andycostanza.archetypes
[INFO] Parameter: version, Value: 1.0-SNAPSHOT
[INFO] Parameter: groupId, Value: com.andycostanza.archetypes
[INFO] Parameter: artifactId, Value: my-spring-boot-archetype
[INFO] Project created from Archetype in dir: /home/andy/projects/my-spring-boot-archetype
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```