+++
title = "Utilisez Gitlab.com pour déployer automatiquement votre site web Hugo"
tags = ["hugo","gitlab","gitlab-ci","git"]
categories = ["Reprendre le contrôle"]
author = "Andy Costanza"
comments = true
layout = "post"
date = "2020-03-29T10:15:50+01:00"
+++
Cela fait maintenant plusieurs mois que j'ai migré mon site de Wordpress à Hugo. Alors, non, je ne vous expliquerai pas qu'est-ce que c'est Hugo car [on l'a déjà fait](https://jamstatic.fr/2019/02/06/de-wordpress-a-hugo-un-nouvel-etat-d-esprit/). Non, je ne vous expliquerai pas pourquoi Hugo c'est mieux que Wordpress car [on l'a déjà fait](https://anceret-matthieu.fr/2020/01/migration-de-mon-blog-wordpress-vers-hugo-sur-gitlab/). Non, je ne vous expliquerai pas non plus comment récupérer le contenu de votre Wordpress car [on l'a déjà fait](https://blog.zwindler.fr/2019/06/10/comment-migrer-de-wordpress-a-hugo/)

Dans cet article je vais vous expliquer comment, grâce au CI de GitLab, j'ai automatisé la publication de mes articles sur un hébergement Web qui dispose d'un accès FTP.

## Prérequis
- Vous disposez d'un compte sur une instance [GitLab](https://gitlab.com)
- Votre instance GitLab dispose d'un GitLab Runner qui se chargera du CI/CD
- Vous avez [installé Hugo, généré votre site et installé votre thème](https://gohugo.io/getting-started/quick-start/)

## Description de processus
Dans notre exemple, nous allons "pusher" sur notre repository GitLab notre site hugo avec le fichier qui décris comment construire et publier notre site web. GitLab va lire ce fichier et démarrer une tâche qui se chargera d'exécuter chaque étape nécessaire à la construction du site pour terminer par publier le site généré sur le serveur FTP de votre hébergement web.

![](http://www.plantuml.com/plantuml/png/PP4zRkCm48NhvIa6RBOotM-nS9MuI64u1b88BYXg9D7XZqAHDep4bqddS-XY6Pc9OqC3292euVlUcrdqKOQOh65IHH_W1Kih-uY15TVNBSo2Rjeu-mzhx7olgV48_w1BVSkShn3ueuvFikJmpW1gRoeCm7zxytakvMtd7Re8_5b7786mJ8tdx4-j3Kc9HaSZovdIucWD1s1mM_HrekSjORfoOPsSm_23wOaf6uJDyAbQ_Bx0Jgou0r8g2bJafYrJngSAU9jAKNdrYa6Gqwn80zArLEHuvtVUwdbxyPiry_rHx8FKRXgsD27jQBnEQGJpLzlHKzQUM16N0GiexjEF6igxgD55Dladn9GOXdCzd0CwDM5IvMKjXtE9eK5BgXSFSeyAUWnxJ04UDglxFHuEXpcdgBTP2JmUZyHLAet6h67gsB45kYfPymK0)

## Le secret du déploiement automatique
GitLab CI dispose de fonctionnalités équivalentes à [Travis-CI](https://about.gitlab.com/devops-tools/travis-ci-vs-gitlab.html) ou même notre bon vieux [Jenkins](https://about.gitlab.com/devops-tools/jenkins-vs-gitlab.html) avec l'avantage d'avoir tout sous la main et directement intégré à son projet.

Voici notre fichier .gitlab-ci.yml de référence :

```yaml
stages:
  - build
  - deploy
build:
  stage: build
  image: jojomi/hugo
  script:
  - hugo version
  - git submodule update --init --recursive
  - hugo -d public_html
  artifacts:
    paths:
    - public_html
  only:
  - master
deploy:
  stage: deploy
  script:
    - apt-get update -qq && apt-get install -y -qq lftp
    - lftp -u $FTP_LOGIN,$FTP_PASSWORD $FTP_HOST -e "mirror -v --parallel=2 -e -R --ignore-time -p ./public_html/ www/ ; quit"
    - echo "deployment complete"
  dependencies:
    - build
```
- Le fichier est découpé en 2 étapes : __build__ et __deploy__
- L'étape __deploy__ dépend de la bonne exécution de l'étape __build__
- L'étape __build__ ne s'exécutera que sur la branche __master__
- Dans __build__ on fait référence à l'image docker [jojomi/hugo](https://hub.docker.com/r/jojomi/hugo) qui sera utilisé pour construire notre site web
- Dans __build__ on exécutera les commandes définies dans __script__ qui se chargeront d'initialisé le thème de notre site et transformera votre site Hugo en site statique HTML dans le dossier __public_html__ du conteneur
- Si tous se passe correctement, on passe à l'étape de __deploy__
- On installe les outils qui nous manque pour uploader des fichiers par FTP
- On termine par le transfert de fichier en tant que tel en publiant le contenu du __public_html__ dans le répertoire __www__ du serveur FTP de votre hébergement web.

### Ajouter les variables de connexion FTP dans votre GitLab
Dans la dernière commande ci-dessus, nous utilisons des variables _$FTP_LOGIN_,_$FTP_PASSWORD_ et _$FTP_HOST_ pour protéger ses valeurs d'une simple lecture du contenu du repository. 

Dans les paramètres de votre repository, rendez-vous dans _Intégration et livraison continue_ et dans la section _Variables_ ajoutez ces 3 noms de variables ainsi que les valeurs et n'oubliez pas de glisser le curseur sur _Protégée_

![](/images/2020-03-29/01.png)

Voilà, vous êtes paré pour publier automatiquement votre site web à chaque push dans votre repository GitLab